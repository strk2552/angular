angular.module('app', ['ui.bootstrap'])
    .filter('sumTotal', function () {
        return function (data) {
            if (angular.isUndefined(data))
                return 0;
            var sum = 0;

            angular.forEach(data, function (item) {
                if (item.type == "Зачисление") {
                    sum += parseFloat(item.total);
                } else {
                    sum -= parseFloat(item.total);
                }
            });
            return (sum).toFixed(2);
        }
    })
    .controller('mainCtrl', ['$scope', '$http', '$uibModal', function ($scope, $http, $uibModal) {
        $scope.categories = [];
        $scope.items = [];
        $scope.nextId = 1;
        $scope.fileters = {
            category: ''
        };
        $scope.order = {
            by: 'id',
            desc: false
        };

        $scope.getCategories = function () {
            $scope.categories = [];
            angular.forEach($scope.items, function (item) {
                if ($scope.categories.indexOf(item.category) == -1) {
                    $scope.categories.push(item.category);
                }
            });
        };

        $scope.getIndex = function (id) {
            for (key in $scope.items) {
                if ($scope.items[key].id == id) {
                    return key;
                }
            }
            return false;
        };

        $scope.sort = function (sortBy) {
            if (sortBy == $scope.order.by) {
                $scope.order.desc = !$scope.order.desc;
            } else {
                $scope.order.by = sortBy;
                $scope.order.desc = false;
            }
        };

        $scope.add = function () {
            var modalInstance = $uibModal.open({
                templateUrl: 'edit.html',
                controller: 'modalCtrl',
                size: 'm',
                resolve: {
                    item: function () {
                        return false ;
                    }
                }
            });

            modalInstance.result.then(function (item) {
                item.id = $scope.nextId++;
                $scope.items.push(item);
                $scope.getCategories();
            }, function () {

            });
        };

        $scope.edit = function (id) {
            if (index = $scope.getIndex(id)) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'edit.html',
                    controller: 'modalCtrl',
                    size: 'm',
                    resolve: {
                        item: function () {
                            return angular.copy($scope.items[index]);
                        }
                    }
                });

                modalInstance.result.then(function (item) {
                    $scope.items[index] = item;
                    $scope.getCategories();
                }, function () {

                });
            }
        };

        $scope.delete = function (id) {
            if (index = $scope.getIndex(id)) {
                $scope.items.splice(index, 1);
            }
        };

        $http.get("data.json").then(function (response) {
            angular.forEach(response.data, function (item) {
                if (item.id >= $scope.nextId) {
                    $scope.nextId = item.id + 1;
                }
                item.date = moment(item.date, "DD.MM.YYYY").toDate();
                $scope.items.push(item);
            });
            $scope.getCategories();
        });
    }])
    .controller('modalCtrl', ['$scope', '$uibModalInstance', 'item', function ($scope, $uibModalInstance, item) {
        $scope.defaultItem = {
            id: null,
            type: "Зачисление",
            category: "",
            total: 0,
            date: new Date()
        };
        $scope.item = item;

        $scope.format = 'dd.MM.yyyy';

        $scope.dateOptions = {
            startingDay: 1,
        };

        if (!item) {
            $scope.item = $scope.defaultItem;
        }

        $scope.save = function () {
            if (!$scope.item.category) {
                $scope.item.category = 'Без категории';
            }
            $uibModalInstance.close($scope.item);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);